# Inteligencia Artificial I 2022-2

## Bienvenidos!

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/raw/master/imgs/banner_IA.png"  width="1000px" height="200px">


## Colaboratory (Google)

Vamos a utilizar la plataforma de google para editar, compartir y correr notebooks: [**Colaboratory**](https://colab.research.google.com/notebooks/welcome.ipynb) 

- Necesitas una cuenta de gmail y luego entras a drive
- Colaboratory es un entorno de notebook de Jupyter gratuito que no requiere configuración y se ejecuta completamente en la nube.
    - Usaremos parte de la infraestructura de computo de google... gratis! (máximo procesos de 8 horas)
- Con Colaboratory, puedes escribir y ejecutar código, guardar y compartir análisis, y acceder a recursos informáticos potentes, todo gratis en tu navegador.
- También puedes usar los recursos de computador Local. 


## Calificación
- 30% Talleres
- 10% Talleres en clase. (>5 participaciones)
- 30% Parciales
- 30% Proyecto funcional IA 

## Talleres (Problemsets)

Los talleres pretenden ser una herramienta practica para afianzar los conocimientos desarrollados durante las clases. En general se presentan como un conjunto de ejercicios que serán desarrollados **individualmente** por los estudiantes. Cada taller esta escrito como un notebook para la validación automática. Se pueden hacer tantos intentos como se quieran y unicamente la última respuesta será tomada en cuenta. Cada uno de los talleres ser desarrollará en casa, dentro de las fechas establecidas en el cronograma. 


## Parciales (Quizes)

Son evaluaciones **individuales** basadas en notebooks sobre los temas tratados en las clases. Los estudiantes deben solucionarlo en el salón de clase, en un tiempo determinado. Los apuntes y notebooks del curso se pueden utilizar (curso del repositorio). 


## Proyecto funcional IA

- **Funcionamiento del proyecto**: El proyecto se debe realizar como un notebook y debe ser 100% funcional.

- **Prototipo (PRE-SUS PROJ)**: En este item se considera como esta estructurado el proyecto y se espera una nivel razonable de funcionalidad.

- **Presentación**:
    - Imagen relacionada (800 x 300) con la siguiente información: título del proyecto e información de los estudiantes<br>
    - Video corto (Máximo 5 minutos) (ENTREGAR EL ARCHIVO DE VIDEO y también alojarlo en youtube)<br>
    - Archivo de las diapositivas

- **Sustentación**: Se realizarán preguntas cortas a los estudiantes unicamente relacionadas con el proyecto. 
 
Todos los items tienen el mismo porcentaje de evaluación. 

- **REPOSITORIO DEL PROYECTO**
    - Todos los archivos relacionados con el proyecto deben alojarse en un repositorio de los integrantes del estudainte
    - El archivo readme.md del repositorio debe tener la siguiente información:
        - Titulo del proyecto
        - Banner- Imagen de 800 x 300
        - Autores: Separados por comas
        - Objetivo: Una frase
        - Dataset: información con link de descarga
        - Modelos: Métodos usados para su desarrollo. Escribir solo palabras claves. 
        - Enlaces del código, video, y repositorio
    


**UNICAMENTE SE TENDRAN EN CUENTA LOS PROYECTOS QUE SE HAYAN POSTULADO AL FINALIZAR EL PRIMER CORTE**


## Calendario y plazos

                        SESSION 1            SESSION 2              SESSION SATURDAY


     W01 oct11-oct12    Intro                Python-general
     W02 oct18-oct19    Python-Numpy         Pandas
     W03 oct25-oct26    Pandas               Visualización
     W04 nov01-nov02    análisis datos       análisis de datos
     W05 nov08-nov09    análisis datos       PROYECTO-AVANCE 1	          Parcial 1 (2 a 4 pm)
     W06 nov15-nov16    Intro & ML-Gauss     Clasificación A.M.S.
     W07 nov22-nov23    Clasificación A.M.S. Metodos A.M.S
     W08 nov29-nov30    Regresión A.M.S      Deep Learning
     W09 dic06-dic07    DNN-imgs-estruc      DNN-audio
     W10 dic13-dic14    DNN-audio            PROYECTO-AVANCE 2

         Dic 19-Ene 06  ------ VACACIONES --------------------




     W11 ene10-ene11  DNN-imgs-estruc         DNN-imgs-estruc
     W12 ene17-ene18  DNN-imgs-estruc         DNN-audio
     W13 ene24-ene25  No supervisado          No supervisado            parcial 2 (2 a 4 pm) - 28 de Enero
     W14 ene31-feb01  Planning and search     Genetic Alg
     W15 feb07-feb08  Genetic Alg             aclaraciones              Parcial 3  (2 a 4 pm)
     W16 feb14-feb15  SUS PROJ                SUS PROJ




     Nov 18             -> Registro primera nota
     Dic 02             -> Último día cancelación materias
     Dic 19 - Ene 06    -> VACACIONES
     Feb 17             -> Finalización clase
     Feb 17 - Feb 21    ->  evaluaciones finales
     Feb 21             -> Registro calificaciones finales
     Feb 24             -> habilitaciones
     Feb 25             -> Registro de calificaciones definitivas

    


**ACUERDO n.° acuerdo 162, del 7 de junio de 2022**
[Calendario academico](https://uis.edu.co/wp-content/uploads/2022/06/Acuacadem-162-22.pdf)


**CUALQUIER ENTREGA FUERA DE PLAZO SERÁ PENALIZADA CON UN 50%**

**LOS PROBLEMSETS ESTAN SUJETOS A CAMBIOS QUE SERÁN DEBIDAMENTE INFORMADOS**

**DEADLINE DE LOS PROBLEMSETS SERÁ EL DIA DE CADA PARCIAL**

