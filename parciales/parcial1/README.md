## **RETO DE ANALÍTICA DE DATOS** (30% del primer parcial)

<!--_Usted es el coordinador regional de Educación de un departamento (puede seleccionar a Santander). ¿Qué se le viene a la mente cuando escucha "Pruebas Icfes"? ¿Cómo les habrá ido a los colegios de mi región?. ¿Qué tipo de mejoras debo hacer para el próximo año?._

_La próxima semana tiene reunión con el Gobernador y usted piensa solicitar el desarrollo de diferentes proyectos educativos (o sociales) para el transcurso del próximo año. Para esto usted debe diseñar un informe gráfico que sea atractivo, y responda preguntas de su propuesta como el "para qué" ?_

_Para ello, utilice el archivo depurado [“filterICFES.csv”](https://drive.google.com/file/d/1w0brUz9iXZTO5mYd7bhihOlLO6k3Xfmg/view?usp=sharing). Descarge el conjunto de datos.  Por favor mire el archivo [“string-to-data”](https://docs.google.com/spreadsheets/d/17XyCR9eSolxmSPR592cWrcFlERx08-TgqQSxzHVGEfM/edit?usp=sharing) para verificar las correspondencias numéricas de algunas filas._

### **Actividades**

1. Seleccione un departamento. Cada estudiante debe seleccionar un departamento y los departamentos no se pueden repetir entre estudiantes.
2. Grafique la correlación general de las pruebas **(1 punto)**
3. Grafique la correlación general de las variables pero sólo de su departamento. Concluya cómo le va a su departamento con respecto a las pruebas generales **(1 punto)**
4. Grafique al menos un histograma, un “violinplot” y un gráfico libre con las variables del icfes que le permitan concluir las fortalezas y desventajas de los estudiantes **en Colombia** **(1 punto)**
5. Grafique al menos un histograma, un “violinplot” y un gráfico libre con las variables del icfes **en su departamento** que le permitan concluir las fortalezas y desventajas de los estudiantes en su departamento con respecto a colombia. Concluya con los requerimientos al gobernador justificado en los datos analizados **(2 puntos)**


- **UNA VEZ REALIZADO EL NOTEBOOK CON EL INFORME (ACTIVIDADES ANTERIORES) DEBE ENVIARLO AL PROFESOR CORRESPONDIENTE POR CORREO**-->

**UNA VEZ REALIZADO EL NOTEBOOK CON EL INFORME DEBE ENVIARLO AL PROFESOR CORRESPONDIENTE POR CORREO**

- Los correos de los profesores son:
    - Fabio Martínez. famarcar@saber.uis.edu.co
    - Gustavo Garzon.  gustavo.garzon@saber.uis.edu.co

- **FECHA LIMITE: JUEVES 17 DE NOVIEMBRE, 6 PM**
