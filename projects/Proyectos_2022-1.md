---
# Proyectos 2021-1. Inteligencia Artificial. 

## Prof: Fabio Martínez, Ph.D
## Prof: Gustavo Garzón
---

# Lista de Proyectos
1. [Segmentación de imagenes MRI - python](#proy1)
2. [Fake news identifier](#proy2)
3. [Detección del número máximo de partículas de una cascada de astropartículas](#proy3)
4. [Predicción de accidentes cerebrovasculares](#proy4)
5. [En busqueda de exoplanetas habitables](#proy5)
6. [Diagnóstico de enfermedades de tiroides usando clasificadores y muestras de pacientes tratados](#proy6)
7. [Reconocimiento de incendios forestales a partir de imágenes RGB](#proy7)
8. [Detección de enfermedades respiratorias](#proy8)
9. [Search Burn](#proy9)

---


## Segmentación de imagenes MRI - python <a name="proy1"></a>

**Autores: Daniel Felipe Castellanos Gélvez, Daniel Felipe Jaimes Blanco, Leider Eduardo Diaz Martínez**

<img src="https://camo.githubusercontent.com/2aae28528c92c0fabd6ab1662646e6c537723176beda457b4865653d786bb31d/68747470733a2f2f63646e2e646973636f72646170702e636f6d2f6174746163686d656e74732f3639313739373139323034313536323132322f313031393032313639303136313836343737352f62616e6e65722e6a7067" style="width:700px;">

**Objetivo:segmentación de las materias gris y blanca en imagenes de resonancia magnetica para la detección de alzheimer**

- Modelo: DecisionTreeClasification, RandomForest, Kmeans

[(code)](https://github.com/RockyCott/DrDTA) [(video)](https://youtu.be/1rvoFp3m7OU) [(+info)](https://github.com/RockyCott/DrDTA/blob/main/dta%20exposicion.pptx)
---

## Fake News Identifier <a name="proy2"></a>

**Autores: Jefrey Steven Torres**

<img src="https://raw.githubusercontent.com/JSteven20/ProyectoIA/main/FakeNewsBanner.jpg" style="width:700px;">

**Objetivo:Identificar noticias falsas que provocan un peligroso círculo de desinformación.**

- Modelo: Estandarización, vectorización, entrenamiento y pruebas
[(code)](https://github.com/JSteven20/ProyectoIA) [(video)](https://drive.google.com/drive/folders/1dHdn_6aKoMNH6Itoakey53tpml8hnDv-) [(+info)](https://drive.google.com/drive/folders/1dHdn_6aKoMNH6Itoakey53tpml8hnDv-)
---

## Detección del número máximo de partículas de una cascada de astropartículas <a name="proy3"></a>

**Autores:José Fredy Navarro,Juan Andrés Guarín Rojas, Brayan Sneider Daza Suárez**

<img src="https://raw.githubusercontent.com/AndresGuarin/Images/main/IA-1/IronProtonXmaxDetectionBanner.png" style="width:700px;">

**Objetivo:Aplicar un modelo de regresión para estimar el número máximo de partículas (MC Xmax).**

- Modelo: DecisionTreeRegressor, RandomForestRegressor, cross_val_score.


[(code)]( https://drive.google.com/file/d/1ulZC5_Xf6kswnCSGph2clLh9ZJ5znNLV/view?usp=sharing) [(video)]( https://youtu.be/PB6hvyfFuvw) [(+info)](https://github.com/AndresGuarin/ML_Estimacion_Xmax/)
---

## Predicción de accidentes cerebrovasculares <a name="proy4"></a>

No asistio
---

## Cirobot y el FerXXo en búsqueda de exoplanetas habitables <a name="proy5"></a>

**Autores:Camilo Ciro, Duvan Vargas, Fabián Bonilla**

<img src="https://user-images.githubusercontent.com/98864594/190227154-5cf2b7a6-60a1-46a9-a70c-2a7ae7c83dff.jpeg" style="width:700px;">

**Objetivo:Buscar planetas potencialmente habitables por medio de los datos registrados por el telescopio keppler**

- Modelo: Aprendizaje no -supervisado


[(code)](https://github.com/NatHreZor/Proyect-IA) [(video)](https://www.youtube.com/watch?v=h4cwDap3JY0) [(+info)](https://github.com/NatHreZor/Proyect-IA)
---

## Diagnóstico de enfermedades de tiroides usando clasificadores y muestras de pacientes tratados <a name="proy6"></a>

**Autores:Jhon Novoa, Miguel Espinel, Yerson Ibarra**

<img src="https://raw.githubusercontent.com/leugimlenipse/AI-thyroid-uis/main/Banner.jpg" style="width:700px;">

**Objetivo:Apoyar diagnósticos de tiroides con clasificadores**

- Modelo: DT


[(code)](https://github.com/leugimlenipse/AI-thyroid-uis) [(video)](https://github.com/leugimlenipse/AI-thyroid-uis) [(+info)](https://github.com/leugimlenipse/AI-thyroid-uis)
---

## Reconocimiento de incendios forestales a partir de imágenes RGB <a name="proy7"></a>

**Autores:Juan pablo Ramirez**

<img src="https://user-images.githubusercontent.com/82167482/190054567-88952ad1-d9f5-4eb1-ba52-2b338eed128a.png" style="width:700px;">

**Objetivo:reconocer un incendio forestal a partir de una foto en formato RGB**
- Modelo: Supervised models


[(code)](https://github.com/JuanPabloRamirezDuarte/Proyecto-IA) [(video)](https://youtu.be/Nch4zbamKSg.) [(+info)](https://github.com/JuanPabloRamirezDuarte/Proyecto-IA)
---

## Detección de enfermedades respiratorias <a name="proy8"></a>

**Autores:Deimer Ivan Castillo Caceres**

<img src="https://raw.githubusercontent.com/deimerin/ai1-project/main/ai-project-banner.png" style="width:700px;">

**Objetivo:Construir un modelo que clasifique enfermedades respiratorias a partir de características de grabaciones de sonidos respiratorios.**
- Modelo: DL
-Data: ICBHI 2017 Challenge Page, kaggle

[(code)](https://github.com/deimerin/ai1-project/) [(video)](https://www.youtube.com/watch?v=U8bbm0il6Fk) [(+info)](https://github.com/deimerin/ai1-project/)
---
##  <a name="proy9"></a>

**Autores:Esneydith Patricia Jaimes Benitez, Dairon Alexis Vallejop Vallejo, David Santiago Morantes Duarte**

<img src="https://github.com/PhantomBlack219/Searching_burns/blob/main/assets/Banner.jpg?raw=true" style="width:700px;">

**Objetivo:Desarrollar e implementar un apoyo para los especialistas medicos e incluso personas del común para identificar y clasificar los tipos de quemaduras en la piel.**
- Modelo: Modelos supervisados

[(code)](https://github.com/PhantomBlack219/Searching_burns) [(video)](https://youtu.be/Zk6MCWfK4mA) [(+info)](https://github.com/PhantomBlack219/Searching_burns)
---
